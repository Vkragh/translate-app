import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Container } from "react-bootstrap";
import { useAuth } from "../../context/AuthContext";
import { authUser } from "../../util/authUser";

import { HomeHero } from "./HomeHero";
import { HomeAuth } from "./HomeAuth";
import { InputForm } from "../Global/Input/InputForm";

export const Home = ({ location }) => {
    const [page, setPage] = useState({});
    const [userName, setUserName] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const { login, signup, currentUser } = useAuth();
    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        authUser(userName, page.action, setLoading, setError, history);
    };

    useEffect(() => {
        if (location.pathname === "/register") {
            setPage({
                linkPath: "/",
                linkText: "Already got a user?",
                action: signup,
            });
        } else {
            setPage({
                linkPath: "/register",
                linkText: "New user?",
                action: login,
            });
        }

        return () => {
            setUserName("");
        };
    }, [location, signup, login]);

    return (
        <Container fluid className='hero-container'>
            <Container>
                {page.linkPath && (
                    <HomeHero path={page.linkPath} link={page.linkText} />
                )}
            </Container>
            {!currentUser ? (
                <div className='login-container rounded shadow d-flex flex-column container justify-content-center align-items-center bg-white'>
                    <InputForm
                        handleSubmit={handleSubmit}
                        inputData={userName}
                        setInputData={setUserName}
                        loading={loading}
                        placeholderText={"What's your name?"}
                        error={error}
                        setError={setError}
                    />
                </div>
            ) : (
                <HomeAuth />
            )}
        </Container>
    );
};
