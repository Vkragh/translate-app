import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const app = firebase.initializeApp({
	apiKey: process.env.REACT_APP_FIREBASE_API_KEY.toString(),
	authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN.toString(),
	projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID.toString(),
	storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET.toString(),
	messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID.toString(),
	appId: process.env.REACT_APP_FIREBASE_APP_ID.toString(),
});

export const auth = app.auth();
export const db = app.firestore();
export default app;
