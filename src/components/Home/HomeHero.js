import React from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";

export const HomeHero = ({ path, link }) => {
    const { currentUser } = useAuth();
    return (
        <div className='row'>
            <div className='col-sm-6'>
                <img
                    className='hero-img'
                    src={process.env.PUBLIC_URL + "/images/Logo.png"}
                    alt='logo'
                />
            </div>
            <div className='col-sm-6'>
                <div className='hero-intro'>
                    <h1 className='title' style={frontPageTitleStyles}>
                        Lost in Translation
                    </h1>
                    <h2 className='title mb-5'>Get started</h2>
                    {!currentUser && (
                        <Link to={path} className='register-link'>
                            {link}
                        </Link>
                    )}
                </div>
            </div>
        </div>
    );
};

const frontPageTitleStyles = {
    fontSize: "3.5rem",
};
