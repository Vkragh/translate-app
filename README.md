# Translate to Sign Language Project

Translate App Developed with ReactJS

Requirements for the App:
Must have three routes

<h3>Startup Route</h3>
<ul>
<li>The first thing a user should see is the “Login page” where the user must be able to enter their name.</li>
</ul>
<h3>Translation route</h3>
<ul>
<li>A user may only view this page if they are currently logged into the app. Please redirect a user back to the login
page if now active login session exists.</li>
<li>The user types in the input box at the top of the page. The user must click on the “translate” button to the right
of the input box to trigger the translation. </li>
<li>The Sign language characters must appear in the “translated” box.</li>
</ul>
<h3>Profile route</h3>
<ul>
<li>The profile page must display the last 10 translations for the current user.</li>
<li>There must also be a button to clear the translations.</li>
</ul>

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
